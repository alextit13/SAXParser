package com.company;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
	    Main app = new Main();
	    app.runTheApp();
    }

    private void runTheApp() {
        File file = new File("resources/test.xml");
        SAXParserFactory parserF = SAXParserFactory.newInstance();
        Handler handler = new Handler();
        try {
            SAXParser saxparser = parserF.newSAXParser();
            saxparser.parse(file,handler);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String, String> data = handler.getData();
        for (Map.Entry<String, String> dMap : data.entrySet()){
            System.out.println(dMap.getKey()+"-"+dMap.getValue());
        }
    }
}
