package com.company.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by java-teacher on 4/11/17.
 */
public class Faculty {
    private String name;
    private List<Student> students;

    public Faculty() {
        this.students = new ArrayList<Student>();
    }

    public Faculty(String name, List<Student> students) {
        this.name = name;
        this.students = students;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Faculty{\n" +
                "\nname='" + name + '\'' +
                ", \nstudents=\n" + students +
                "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Faculty faculty = (Faculty) o;

        if (name != null ? !name.equals(faculty.name) : faculty.name != null) return false;
        return students != null ? students.equals(faculty.students) : faculty.students == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (students != null ? students.hashCode() : 0);
        return result;
    }
}
