package com.company.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by java-teacher on 4/11/17.
 */
public class Univer {
    private String name;
    private List<Faculty> faculties;

    public Univer() {
        this.faculties = new ArrayList<Faculty>();
    }

    public Univer(String name, List<Faculty> faculties) {
        this.name = name;
        this.faculties = faculties;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Faculty> getFaculties() {
        return faculties;
    }

    public void setFaculties(List<Faculty> faculties) {
        this.faculties = faculties;
    }

    @Override
    public String toString() {
        return "Univer{\n" +
                "\nname='" + name + '\'' +
                ", \nfaculties=\n" + faculties +
                "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Univer univer = (Univer) o;

        if (name != null ? !name.equals(univer.name) : univer.name != null) return false;
        return faculties != null ? faculties.equals(univer.faculties) : univer.faculties == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (faculties != null ? faculties.hashCode() : 0);
        return result;
    }
}