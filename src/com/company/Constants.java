package com.company;

/**
 * Created by java-teacher on 4/11/17.
 */
public class Constants {
    public static final String XML_ELEMENT_UNIVER = "univer";
    public static final String XML_ELEMENT_FACULTIES = "faculties";
    public static final String XML_ELEMENT_FACULTY = "faculty";
    public static final String XML_ELEMENT_STUDENTS = "students";
    public static final String XML_ELEMENT_STUDENT = "student";

    public static final String XML_ATTRIBUTE_NAME = "name";
    public static final String XML_ATTRIBUTE_FIRST_NAME = "firstname";
    public static final String XML_ATTRIBUTE_LAST_NAME = "lastname";
}
