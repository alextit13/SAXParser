package com.company;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import java.util.HashMap;
import java.util.Map;

public class Handler extends DefaultHandler {
    private String firstname;
    private String lastname;
    private Map<String, String> data = new HashMap<String, String>();
    private String element;

    @Override
    public void startDocument()throws SAXException{
        System.out.println("start parsing...");
    }

    @Override
    public void endDocument()throws SAXException{
        System.out.println("end parsing...");
    }

    @Override
    public void startElement(String namespace,
                             String localName,
                             String qName,
                             Attributes attr){
        element = qName;
        if (element.equals("student")){
            firstname = attr.getValue(0);
        }
    }

    @Override
    public void endElement(String namespace,
                           String localName,
                           String qName){
        element = "";
    }

    @Override
    public void characters(char[]ch,
                           int start,
                           int end){
        if (element.equals("faculty")){
            String str = new String(ch,start,end);
        }
        data.put(firstname,lastname);
    }

    public Map<String,String> getData(){
        return data;
    }
}
